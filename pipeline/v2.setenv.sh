#!/bin/bash
set -e
# workflow
# store => load => load => load
# store => load , update => load => load
# store => load , update => load , update => load

function set_variables () 
{

    if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then
        echo "Updating environment variables"
        # desencryption
        openssl rsautl -decrypt -inkey $CI_PROJECT_DIR/keys/id_rsa -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin
        #add the new values to the file
        for var in "$@"
        do
        echo $var >> $CI_PROJECT_DIR/variables.env.tmp # not use echo "$var" => this cause "json expands"
        done
        # delete old encryp-file
        rm -rf $CI_PROJECT_DIR/variables.env.enc
        # encryp the file again
        openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
        openssl rsautl -encrypt -inkey $CI_PROJECT_DIR/keys/id_rsa.pem.pub -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc
        # delete sensitibe information
        rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp # delete sensitive information
    else
        echo "settings environment variables"
        for var in "$@"
        do
        echo $var >> $CI_PROJECT_DIR/variables.env.tmp
        done

        #It is NOT possible encrypt larges files with openssl rsatl
        #openssl rsautl -encrypt -pubin -inkey $CI_PROJECT_DIR/keys/id_rsa.pem.pub -ssl -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env
        openssl rand -base64 32 > key.bin
        openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
        openssl rsautl -encrypt -inkey $CI_PROJECT_DIR/keys/id_rsa.pem.pub -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc
        # delete sensitive information
        rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp 

    fi

}

function load_variables ()
{
    if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then

        #It is NOT possible encrypt larges files with openssl rsatl
        #openssl rsautl -decrypt -inkey $CI_PROJECT_DIR/keys/id_rsa -in $CI_PROJECT_DIR/variables.env -out $CI_PROJECT_DIR/variables.env.tmp

        openssl rsautl -decrypt -inkey $CI_PROJECT_DIR/keys/id_rsa -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin

        #add automatically backslash bedore each quote \" (JSON FILE)
        sed -i 's/"/\\"/g' $CI_PROJECT_DIR/variables.env.tmp

        while IFS= read -r line; do
            key=$( echo $line | cut -d "=" -f1 )
            value=$( echo $line | cut -d "=" -f2- )
            finalValue=$( echo $key=\"$value\" )
            eval export $finalValue
        done < $CI_PROJECT_DIR/variables.env.tmp

        # delete sensitive data
        rm -f $CI_PROJECT_DIR/variables.env.tmp $CI_PROJECT_DIR/key.bin 

    else
        echo " There are NOT environment variables to load "

    fi

}


function set_files () 
{

        echo "Updating  files"
        # desencryption
        openssl rsautl -decrypt -inkey $CI_PROJECT_DIR/keys/id_rsa -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin
        #add the new values to the file
        for var in "$@"
        do
        echo $var >> $CI_PROJECT_DIR/variables.env.tmp # not use echo "$var" => this cause "json expands
        done
        # delete old encryp-file
        rm -rf $CI_PROJECT_DIR/variables.env.enc
        # encryp the file again
        openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
        openssl rsautl -encrypt -inkey $CI_PROJECT_DIR/keys/id_rsa.pem.pub -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc
        # delete sensitibe information
        rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp # delete sensitive information
    else
        echo "encrypting file "
        # unique key for encrypt all the files
        openssl rand -base64 32 > key.file.bin 
        # read arguments
        for var in "$@"
        do
        #It is NOT possible encrypt larges files with openssl rsatl
        #openssl rsautl -encrypt -pubin -inkey $CI_PROJECT_DIR/keys/id_rsa.pem.pub -ssl -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env
        #openssl rand -base64 32 > key.file.bin
        directory=$( dirname $CI_PROJECT_DIR/$var )
        filename=$( echo $var | rev | cut -d "/" -f 1 | rev)
        openssl enc -aes-256-cbc -salt -in ${directory}/$filename -out ${$CI_PROJECT_DIR}/${filename}.file.enc -pass file:$CI_PROJECT_DIR/key.file.bin
        openssl rsautl -encrypt -inkey $CI_PROJECT_DIR/keys/id_rsa.pem.pub -pubin -in $CI_PROJECT_DIR/key.file.bin -out $CI_PROJECT_DIR/key.file.bin.enc
        # delete sensitive information
        rm -f $CI_PROJECT_DIR/key.file.bin #${directory}/$filename
        #echo $var >> $CI_PROJECT_DIR/variables.env.tmp
        done

    fi




}

function load_files () 
{

    if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then

        #It is NOT possible encrypt larges files with openssl rsatl
        #openssl rsautl -decrypt -inkey $CI_PROJECT_DIR/keys/id_rsa -in $CI_PROJECT_DIR/variables.env -out $CI_PROJECT_DIR/variables.env.tmp

        openssl rsautl -decrypt -inkey $CI_PROJECT_DIR/keys/id_rsa -in $CI_PROJECT_DIR/key.file.bin.enc -out $CI_PROJECT_DIR/key.file.bin


        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin

       

        # delete sensitive data
        rm -f $CI_PROJECT_DIR/variables.env.tmp $CI_PROJECT_DIR/key.bin 
 
    else
        echo " There are NOT files to load "

    fi

}