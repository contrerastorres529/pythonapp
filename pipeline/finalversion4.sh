#!/bin/bash
set -e
## TRAP linux command doesn't work in gitlab-runner =>  https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3009
function set_variables () 
{
    if [ $# -eq 0 ]; then
            echo "There are NOT environment variables to set"
    else
        if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then
            echo "Setting or Updating environment variables"
            # desencryption
            desencryption
            #add the new values to the file
            for var in "$@"
            do
                echo $var >> $CI_PROJECT_DIR/variables.env.tmp # not use echo "$var" => this cause "json expands"
            done
            # detele empty lines
            sed -i  '/^$/d' $CI_PROJECT_DIR/variables.env.tmp
            # delete old encryp-file
            rm -rf $CI_PROJECT_DIR/variables.env.enc
            # encryp the file again
            encryption
            # delete sensitibe information
            rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp # delete sensitive information
        else
            echo "Settings environment variables"
            for var in "$@"
            do
                echo $var >> $CI_PROJECT_DIR/variables.env.tmp
            done
            # detele empty lines
            sed -i  '/^$/d' $CI_PROJECT_DIR/variables.env.tmp
            #Generate main key
            openssl rand -base64 32 > key.bin
            encryption
            # delete sensitive information
            rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp 
        fi
    fi
}
function load_variables ()
{
    if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then
        desencryption
        #add automatically backslash before each quote " =>  \" (JSON FILE)
        sed -i 's/"/\\"/g' $CI_PROJECT_DIR/variables.env.tmp
        while IFS= read -r line; do
            key=$( echo $line | cut -d "=" -f1 )
            value=$( echo $line | cut -d "=" -f2- )
            finalValue=$( echo $key=\"$value\" )
            eval export $finalValue
        done < $CI_PROJECT_DIR/variables.env.tmp
        # delete sensitive data
        rm -f $CI_PROJECT_DIR/variables.env.tmp $CI_PROJECT_DIR/key.bin 
    else
        echo " There are NOT environment variables to load "
    fi
}
function encryption ()
{
    if [ "${CI_COMMIT_REF_PROTECTED}" == "false" ]; then
        openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
        openssl rsautl -encrypt -inkey ${NONPROD_ENV_VARS_PUBLIC_KEY} -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc
    else
        openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
        openssl rsautl -encrypt -inkey ${ENV_VARS_PUBLIC_KEY} -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc
    fi
}
function desencryption ()
{
    if [ "${CI_COMMIT_REF_PROTECTED}" == "false" ]; then
        openssl rsautl -decrypt -inkey ${NONPROD_ENV_VARS_PRIVATE_KEY} -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin
    else
        openssl rsautl -decrypt -inkey ${ENV_VARS_PRIVATE_KEY} -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin
    fi
}
