#!/bin/bash
set -e
# workflow
# set_variables => load_variables
# set_variables  => set_variables ( set new or update old values) => load_variables


## TRAP linux command doesn't work in gitlab-runner =>  https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3009

function set_variables () 
{

    if [ $# -eq 0 ]; then
            echo "There are NOT environment variables to set"
    else
        if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then
            echo "Setting - Updating environment variables"

            # desencryption
            openssl rsautl -decrypt -inkey ${PRIVATE_KEY} -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
            openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin

            #add the new values to the file
            for var in "$@"
            do
                echo $var >> $CI_PROJECT_DIR/variables.env.tmp # not use echo "$var" => this cause "json expands"
            done

            # detele empty lines
            sed -i  '/^$/d' $CI_PROJECT_DIR/variables.env.tmp

            # delete old encryp-file
            rm -rf $CI_PROJECT_DIR/variables.env.enc

            # encryp the file again
            openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
            openssl rsautl -encrypt -inkey ${PUBLIC_KEY} -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc

            # delete sensitibe information
            rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp # delete sensitive information

        else

            echo "Settings environment variables"
            for var in "$@"
            do
                echo $var >> $CI_PROJECT_DIR/variables.env.tmp
            done

            # detele empty lines
            sed -i  '/^$/d' $CI_PROJECT_DIR/variables.env.tmp

            #Encrypt file
            openssl rand -base64 32 > key.bin
            openssl enc -aes-256-cbc -salt -in $CI_PROJECT_DIR/variables.env.tmp -out $CI_PROJECT_DIR/variables.env.enc -pass file:$CI_PROJECT_DIR/key.bin
            openssl rsautl -encrypt -inkey ${PUBLIC_KEY} -pubin -in $CI_PROJECT_DIR/key.bin -out $CI_PROJECT_DIR/key.bin.enc

            # delete sensitive information
            rm -f $CI_PROJECT_DIR/key.bin $CI_PROJECT_DIR/variables.env.tmp 

        fi
    fi

}

function load_variables ()
{
    if [ -f $CI_PROJECT_DIR/variables.env.enc ] && [ -f $CI_PROJECT_DIR/key.bin.enc ]; then

        openssl rsautl -decrypt -inkey ${PRIVATE_KEY} -in $CI_PROJECT_DIR/key.bin.enc -out $CI_PROJECT_DIR/key.bin
        openssl enc -d -aes-256-cbc -in $CI_PROJECT_DIR/variables.env.enc  -out $CI_PROJECT_DIR/variables.env.tmp -pass file:$CI_PROJECT_DIR/key.bin

        #add automatically backslash before each quote " =>  \" (JSON FILE)
        sed -i 's/"/\\"/g' $CI_PROJECT_DIR/variables.env.tmp

        while IFS= read -r line; do
            key=$( echo $line | cut -d "=" -f1 )
            value=$( echo $line | cut -d "=" -f2- )
            finalValue=$( echo $key=\"$value\" )
            eval export $finalValue
        done < $CI_PROJECT_DIR/variables.env.tmp

        # delete sensitive data
        rm -f $CI_PROJECT_DIR/variables.env.tmp $CI_PROJECT_DIR/key.bin 

    else

        echo " There are NOT environment variables to load "
        
    fi

}


