#!/bin/bash
set -e

## TRAP linux command doesn't work in gitlab-runner =>  https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3009

#Load Keys into the keyring

if [ "${CI_COMMIT_REF_PROTECTED}" == "false" ]; then
    gpg --import --batch --trust-model always ${NONPROD_ENV_PUBLIC_GPG} ${NONPROD_ENV_PRIVATE_GPG}
else
    gpg --import --batch --trust-model always ${ENV_PUBLIC_GPG} ${ENV_PRIVATE_GPG}
fi



function set_variables () 
{
    local KEYID

    if [ -z ${ENV_KEYS_EMAIL} ]; then 
        #Keyring with only one key pair. (deafult)
        KEYID=`gpg --dry-run --list-public-keys --batch --with-colons | awk -F: '/^pub:/ { print $5 }'`
    else
        #Keyring with more than one key pair.
        #It is  mandatory set up the user email
        KEYID=`gpg --dry-run --list-public-keys --batch --with-colons ${ENV_KEYS_EMAIL} | awk -F: '/^pub:/ { print $5 }'`
    fi

    if [ $# -eq 0 ]; then

            echo "There are NOT environment variables to set"
    else
        if [ -f $CI_PROJECT_DIR/env.variables.enc ]; then

            echo "Setting or Updating environment variables"

            #desencryption
            if [ "${CI_COMMIT_REF_PROTECTED}" == "false" ]; then

                if [ -z ${NONPROD_ENV_PASSPHRASE} ]; then 
                    gpg --decrypt --batch --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback  $CI_PROJECT_DIR/env.variables.enc
                else
                    gpg --decrypt --batch --passphrase "${NONPROD_ENV_PASSPHRASE}" --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback  $CI_PROJECT_DIR/env.variables.enc
                fi

            else 

                if [ -z ${ENV_PASSPHRASE} ]; then
                    gpg --decrypt --batch --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback   $CI_PROJECT_DIR/env.variables.enc
                else
                    gpg --decrypt --batch --passphrase "${ENV_PASSPHRASE}" --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback   $CI_PROJECT_DIR/env.variables.enc
                fi

            fi
            #add the new values to the file
            for var in "$@"
            do
                echo $var >> $CI_PROJECT_DIR/env.variables.tmp # not use echo "$var" => this cause "json expands"
            done
            # detele empty lines
            sed -i  '/^$/d' $CI_PROJECT_DIR/env.variables.tmp
            # delete old encryp-file
            rm -rf $CI_PROJECT_DIR/env.variables.enc
            #encryption
            gpg --encrypt --batch --trust-model always  -o $CI_PROJECT_DIR/env.variables.enc --recipient ${KEYID} $CI_PROJECT_DIR/env.variables.tmp
            rm -f $CI_PROJECT_DIR/env.variables.tmp 

        else

            echo "Settings environment variables"
            for var in "$@"
            do
                echo $var >> $CI_PROJECT_DIR/env.variables.tmp
            done
            # detele empty lines
            sed -i  '/^$/d' $CI_PROJECT_DIR/env.variables.tmp
            #encryption
            gpg --encrypt --batch --trust-model always  -o $CI_PROJECT_DIR/env.variables.enc --recipient ${KEYID} $CI_PROJECT_DIR/env.variables.tmp
            rm -f $CI_PROJECT_DIR/env.variables.tmp 

        fi
    fi
}


function load_variables ()
{
    if [ -f $CI_PROJECT_DIR/env.variables.enc ]; then

        #desencryption
        if [ "${CI_COMMIT_REF_PROTECTED}" == "false" ]; then

            if [ -z ${NONPROD_ENV_PASSPHRASE} ]; then 
                gpg --decrypt --batch --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback  $CI_PROJECT_DIR/env.variables.enc
            else
                gpg --decrypt --batch --passphrase "${NONPROD_ENV_PASSPHRASE}" --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback  $CI_PROJECT_DIR/env.variables.enc
            fi

        else

            if [ -z ${ENV_PASSPHRASE} ]; then
                gpg --decrypt --batch --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback   $CI_PROJECT_DIR/env.variables.enc
            else
                gpg --decrypt --batch --passphrase "${ENV_PASSPHRASE}" --trust-model always --out $CI_PROJECT_DIR/env.variables.tmp --pinentry-mode loopback   $CI_PROJECT_DIR/env.variables.enc
            fi

        fi
        #add automatically backslash before each quote " =>  \" (JSON FILE)
        sed -i 's/"/\\"/g' $CI_PROJECT_DIR/env.variables.tmp
        while IFS= read -r line; do
            key=$( echo $line | cut -d "=" -f1 )
            value=$( echo $line | cut -d "=" -f2- )
            finalValue=$( echo $key=\"$value\" )
            eval export $finalValue
        done < $CI_PROJECT_DIR/env.variables.tmp
        # delete sensitive data
        rm -f $CI_PROJECT_DIR/env.variables.tmp 
    else
        echo " There are NOT environment variables to load "
    fi
}